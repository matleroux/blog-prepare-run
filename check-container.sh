#!/bin/bash

# Naming arguments
containerName=$1
userName=$2
contextName=$3
name=$4
watch=$5
theme=$6
url=$7
proto=$8
contextReplace="${contextName}_change"

function notNginx() {
  # Check if project is in the /tmp folder
  # If yes, delete it then clone it 
  if [ -d /tmp/$containerName ]
  then
    echo "Folder $containerName exist in /tmp."
    sudo rm -fr /tmp/$containerName
  fi
  cd /tmp
  git clone git@gitlab.com:matleroux/$containerName.git

  # Check if the project is in the user's home
  # If yes, delete it 
  if [ -d /opt/$userName/$containerName ]
  then
    echo "Folder $containerName exist in /opt/$userName."
    sudo rm -fr /opt/$userName/$containerName
  fi

  # Copy the project to the user's home
  sudo cp -R /tmp/$containerName /opt/$userName/

  # Delete the project from /tmp
  sudo rm -fr /tmp/$containerName

  # If the project contain acme.json (and so related to Traefik here), we need to chmod acme.json
  if [ -f "/opt/$userName/$containerName/conf/acme.json" ]
  then
    echo "ACME for Traefik conf found, CHMOD it."
    sudo chmod 0600 /opt/$userName/$containerName/conf/acme.json
  fi

  # Go to the project folder
  cd /opt/$userName/$containerName

  # If docker-compose.yml contain $contextName_change, replace it with the containerName
  if [ -f "/opt/$userName/$containerName/docker-compose.yml" ]
  then
    if grep -q $contextReplace "docker-compose.yml"
    then
      sudo sed -i 's/'$contextReplace'/'$containerName'/g' /opt/$userName/$containerName/docker-compose.yml
    fi
  else
    echo "No docker-compose.yml in /opt/$userName/$contextName-$name/ !"
  fi

  if [ -f "/opt/$userName/$contextName-$name/.env" ]
  then
    sudo sed -i 's/watch_change/'$watch'/g' /opt/$userName/$containerName/.env
    sudo sed -i 's/theme_change/'$theme'/g' /opt/$userName/$containerName/.env
    sudo sed -i 's/url_change/'$url'/g' /opt/$userName/$containerName/.env
    sudo sed -i "s%proto_change%${proto}%g" /opt/$userName/$containerName/.env
    sudo sed -i 's/name_change/'$name'/g' /opt/$userName/$containerName/.env
  else
    echo "No .env in /opt/$userName/$contextName-$name/ !"
  fi
  # Start the container
  docker-compose up -d --remove-orphans $containerName

  # Chown the project
  sudo chown -R $userName:users /opt/$userName/$containerName
}

function isNginx() {
  # Check if docker-compose.yml is in the main project
  if [ -f "/opt/$userName/$contextName-$name/docker-compose.yml" ]
  then
    echo "docker-compose.yml found in $contextName, will start $containerName."

    # Go to the project's home, if docker-compose.yml contain nginx_change, replace it with the containerName
    cd /opt/$userName/$contextName-$name/
    if grep -q nginx_change "docker-compose.yml"; then
      sudo sed -i 's/nginx_change/'$containerName'/g' /opt/$userName/$contextName-$name/docker-compose.yml
    fi

    # Start the container
    docker-compose up -d --remove-orphans $containerName
  else
    echo "Issue with $containerName: docker-compose.yml not found in /opt/$userName/$contextName-$name"
  fi

  # Chown the project
  sudo chown -R $userName:users /opt/$userName/$contextName-$name
}

function checkDir() {
  # We fisrt need to check for /opt
  if [ -d /opt/$userName ]
    then
      echo ">>> /opt/$userName exist"

      # Chown the project
      sudo chown -R $userName:users /opt/$userName
    else
      echo ">>> /opt/$userName does not exist, will create it"
      sudo mkdir /opt/$userName

      # Chown the project
      sudo chown -R $userName:users /opt/$userName
  fi
}

# If not nginx
if [[ "$containerName" != *"nginx"* ]]
then
  checkDir
  echo ">>> $containerName != nginx"
  # Check if container is just exited
  if [ "$(docker ps -aq -f status=exited -f name=$containerName)" ]
  then
    echo ">>> $containerName exited"
    # If exited, check if folder exist and start
    if [ -d /opt/$userName/$containerName ]
    then
      echo ">>> /opt/$userName/$containerName exist"
      cd /opt/$userName/$containerName
      docker-compose up -d --remove-orphans $containerName

      # Chown the project
      sudo chown -R $userName:users /opt/$userName/$contextName-$name
    else
      echo ">>> /opt/$userName/$containerName does not exist, launching function"
      notNginx
    fi
  # Other than exited, clean and create all
  else
    echo ">>> $containerName does not exist, launching function"
    notNginx
  fi
fi

# If nginx
if [[ "$containerName" == *"nginx"* ]]
then
  echo ">>> $containerName == nginx"
  # Check if container is just exited
  if [ "$(docker ps -aq -f status=exited -f name=$containerName)" ]
  then
    echo ">>> $containerName exited"
    # If exited, check if folder exist and start nginx
    if [ -d /opt/$userName/$contextName-$name ]
    then
      echo ">>> /opt/$userName/$contextName-$name exist"
      cd /opt/$userName/$contextName-$name
      docker-compose up -d --remove-orphans $containerName

      # Chown the project
      sudo chown -R $userName:users /opt/$userName/$contextName-$name
    else
      echo ">>> /opt/$userName/$contextName-$name does not exist, launching function"
      notNginx
    fi
  # Other than exited, clean and create all
  else
    echo ">>> $containerName does not exist, launching function"
    isNginx
  fi
fi